package latihanArsip;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Ade Pranaya
 * @since Nov 27, 2017
 */
public class BacaFile {
    public static void main(String[] args) {
        BufferedInputStream bis = null;
        
        try {
            //Membuka File
            bis = new BufferedInputStream(new FileInputStream("myfiles.txt"));
            System.out.println("File Tersedia");
            
            int data = bis.read();
            while (data != -1) {
                char ch = (char)data;
                System.out.print(ch);
                data = bis.read();
            }
            
            //Menutup file
            bis.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }
}
