package latihanArsip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Ade Pranaya
 * @since Nov 27, 2017
 */
public class PengelolaanFile {

    BufferedInputStream bis = null;
    BufferedOutputStream bos = null;
    boolean sign;

    public void openFileBaca(String namaFile) {
        try {
            bis = new BufferedInputStream(new FileInputStream(namaFile));
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    public void openFileTulis(String namaFile) {
        try {
            bos = new BufferedOutputStream(new FileOutputStream(namaFile));
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    boolean bacaFile() {
        sign = true;
        try {
            if (bis != null) {
                int data = bis.read();
                while (data != -1) {
                    char ch = (char) data;
                    System.out.print(ch);
                    data = bis.read();
                }
                System.out.println();
                bis.close();
            }
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            sign = false;
        }
        return sign;
    }

    boolean tulisFile(String isiFile) {
        sign = true;
        try {
            bos.write(isiFile.getBytes());
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            sign = false;
        }
        return sign;
    }

    boolean copyFile() {
        sign = true;
        try {
            if (bis != null) {
                int data = bis.read();
                while (data != -1) {
                    bos.write(data);
                    data = bis.read();
                }
                System.out.println();
                bis.close();
            }
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            sign = false;
        }
        return sign;
    }
    boolean addContentsFile() {
        sign = true;
        StringBuilder sb = new StringBuilder();
        Scanner sc = new Scanner(System.in);
        System.out.println("apakah anda ingin menambah isi file? Y/N");
        while (sc.next().equalsIgnoreCase("y")) {
            System.out.print("Masukkan String : ");
            sb.append(sc.next() + sc.nextLine()+"\n");
            System.out.println("tambah Lagi?");
        }
        
        try {   
                bos.write(sb.toString().getBytes());
                System.out.println();
                bis.close();
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            sign = false;
        }
        return sign;
    }

    public void closeFileBaca() {
        try {
            if (bis != null) {
                bis.close();
            }
        } catch (IOException ioe) {
        }
    }

    public void closeFileTulis() {
        try {
            if (bos != null) {
                bos.close();
            }
        } catch (IOException ioe) {
        }
    }

    public static void main(String[] args) {
        //baca file
        PengelolaanFile pf = new PengelolaanFile();
        pf.openFileBaca("myfiles.txt");
        pf.bacaFile();
        pf.closeFileBaca();
        
        //tulis file
        pf.openFileTulis("myfiles.txt");
        pf.tulisFile("yuhuu");
        pf.closeFileTulis();
        
        //copy file
        pf.openFileBaca("myfiles.txt");
        pf.openFileTulis("hasil_copy_myfiles.txt");
        pf.copyFile();
        pf.closeFileBaca();
        pf.closeFileTulis();
        
        //tambah content file
        /* copy terlebih dahulu file asli ke file sementara*/
        pf.openFileBaca("myfiles.txt");//file asli
        pf.openFileTulis("myfiles_temp.txt");//file sementara
        pf.copyFile();
        pf.closeFileBaca();
        pf.closeFileTulis();
        
        /* hasilfile sementara tadi dicopy kembali ke file asli
        serta tulis file untuk menambah content baru*/
        pf.openFileBaca("myfiles_temp.txt");//file sementara
        pf.openFileTulis("myfiles.txt");//file asli
        pf.copyFile();
        pf.tulisFile("baru ditambahkan"+"\n");
        pf.closeFileBaca();
        pf.closeFileTulis();
        new File("myfiles_temp.txt").delete();//optional
        
        //baca file yang di copy
        pf.openFileBaca("hasil_copy_myfiles.txt");
        pf.bacaFile();
        pf.closeFileBaca();
    }
}
