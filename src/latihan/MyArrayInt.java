package latihan;

import java.util.Scanner;

/**
 *
 * @author Ade Pranaya
 * @since Sep 22, 2017
 */
public class MyArrayInt {

    int[] himpunan = new int[7];
    
    void isiHimpunan() {
        himpunan[0] = 1;
        himpunan[1] = 3;
        himpunan[2] = 4;
        himpunan[3] = 4;
    }

    void tampilkanHimpunan() {
        for (int i = 0; i < himpunan.length; i++) {
            System.out.println("himpunan [" + i + "] " + himpunan[i]);
        }
    }

    int pencarianBinary(int cari) {
        int awal = 0;
        int tengah = 0;
        int akhir = himpunan.length;

        while (awal <= akhir) {
            tengah = (awal + akhir) / 2;
            if (himpunan[tengah] == cari) {
                return tengah;
            } else if (himpunan[tengah] < cari) {
                awal = tengah + 1;
            } else {
                akhir = tengah - 1;
            }
        }
        return -1;
    }
    
    public void pencarianSequential(int cari) {
        String hasil = "";

        boolean ketemu = false;

        for (int i = 0; i < himpunan.length; i++) {
            if (himpunan[i] == cari) {
                ketemu = true;
                if(hasil==""){
                    hasil = hasil + " " + i;
                }else if((i+1)<himpunan.length){
                    hasil = hasil + ", " + i;
                }else {
                    hasil = hasil + " & " + i;
                }
            }
        }
        if (ketemu == true) {
            System.out.println("Data Ada Di Index Ke :" + hasil);
        } else {
            System.out.println("Data Tidak Ditemukan");
        }
    }
}
